const MongoClient = require("mongodb");
const { ipcMain } = require('electron');
const PROTOCOL = "mongodb://";
const log = require('electron-log');

ipcMain.on('mongo-connection', (event, arg) => {
  log.info(arg);
  mongoConnection(event, arg);
})

async function mongoConnection(event, object) {
  try {
    const url = PROTOCOL + object.hostname + ":" + object.port;
    log.info(url);
    const client = await MongoClient.connect(url);
    const db = client.db(object.name);
    db.listCollections().toArray(function (err, items) {
      const collections = items.map((item) => {
        return item['name'];
      });
      event.sender.send('mongo-connection-success', collections);
      client.close();
    });
  } catch (e) {
    log.error(e);
    event.sender.send('mongo-connection-err', e);
  }

}