import React, { useEffect, useCallback } from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import { useDispatch } from 'react-redux';
// const { ipcRenderer } = window.require('electron');

export default function PopUpConnection(props) {
  const [open, setOpen] = React.useState(props.open);
  const [inputs, setInputs] = React.useState({ name: '', hostname: '', port: '' });
  const dispatch = useDispatch();
  
  const addConnection = () => {
    window.ipcRenderer.send('mongo-connection', inputs);
    window.ipcRenderer.on('mongo-connection-success', (event, arg) => {
      console.log('SUCCESS ', arg);
      addConnectionSecondStep(arg);
    });
    window.ipcRenderer.on('mongo-connection-err', (event, arg) => {
      console.log('ERROR ', arg);
    });
  }

  const addConnectionSecondStep = useCallback((collections) => {
    let connection = inputs;
    connection.collections = collections;
    dispatch({ type: 'ADD_CONNECTION', connection: inputs})
  }, [dispatch, inputs]);

  const handleInputsChange = (e) => {
    e.persist();
    setInputs({
      ...inputs,
      [e.target.name]: e.target.value,
    })
  };

  useEffect(() => {
    setOpen(props.open)
  }, [props])

  function handleClose() {
    setOpen(false);
    props.onClose();
  }

  function emptyFields() {
    for (const key in inputs) {
      if (inputs[key] === '') return false;
    }
    return true;
  }

  return (
    <div>
      <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Add new MongoDB connection</DialogTitle>
        <DialogContent>
          <TextField
            autoFocus
            margin="dense"
            name="name"
            label="Database name"
            type="text"
            fullWidth
            value={inputs.name}
            onChange={handleInputsChange}
          />
          <TextField
            margin="dense"
            name="hostname"
            label="Hostname"
            placeholder="localhost"
            type="text"
            fullWidth
            InputLabelProps={{
              shrink: true,
            }}
            value={inputs.hostname}
            onChange={handleInputsChange}
          />
          <TextField
            margin="dense"
            name="port"
            label="Port"
            placeholder="27017"
            type="text"
            fullWidth
            InputLabelProps={{
              shrink: true,
            }}
            value={inputs.port}
            onChange={handleInputsChange}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Cancel
          </Button>
          <Button onClick={addConnection} color="primary" disabled={!emptyFields()}>
            Connect
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}