import React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import NavBar from './components/NavBar/NavBar';

import './App.css';

function App(props) {

  return (
    <div>
      <CssBaseline />
      <NavBar></NavBar>
      <main className="main-content">
        Here content
      </main>
    </div>);

}

export default App;
