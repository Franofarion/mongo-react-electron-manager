import * as actionTypes from '../constants/ActionsTypes';

const initialState = [{ name: 'My database', hostname: 'localhost',  port:'27017', collections: ['example']}];

export default (state = initialState, action) => {
    switch (action.type){
      case actionTypes.ADD_CONNECTION:
        return [
            ...state,
            Object.assign({}, action.connection)
        ];
      default:
            return state;
    }
  };