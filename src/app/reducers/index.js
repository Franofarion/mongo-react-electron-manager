import { combineReducers } from 'redux';
import connections from './connectionsReducer';

export default combineReducers({
    connections: connections
});