import * as types from '../constants/ActionsTypes'

export const addConnection = (connection) => { return { type: types.ADD_CONNECTION, connection: connection }}
// export const previousPage = number => ({ type: types.PREVIOUS_PAGE, number })
